package br.com.softsite.sbd;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

	@GetMapping(path = "{nome}", produces = "text/plain")
	public String hello(@PathVariable("nome") String nome) {
		return "hello, " + nome;
	}
}
