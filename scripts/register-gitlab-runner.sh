#!/bin/bash

docker run --rm -i -t \
        -v `pwd`/:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock \
        gitlab/gitlab-runner register --run-untagged --registration-token `cat token` \
        --tag-list dind -n --executor docker --name "`whoami`-`uname -n`" --url https://gitlab.com/ \
        --docker-image alpine:latest --docker-privileged
