#!/bin/bash

docker run --rm -i -t \
        -v `pwd`/:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock \
        gitlab/gitlab-runner run
